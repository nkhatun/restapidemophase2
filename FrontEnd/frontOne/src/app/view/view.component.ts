import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { RestApiService } from '../rest-api.service';
import { MatChipInputEvent } from '@angular/material/chips';
import { COMMA, ENTER } from '@angular/cdk/keycodes';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {
  isEdit: boolean;
  isView: boolean;
  personId: number;
  registerForm: FormGroup;
  fetchByIdApiURL = 'http://localhost:8080/app/api/fetchPersonById?id=';
  updateApiURL = 'http://localhost:8080/app/api/updatePerson';

  personDataArray: any[] = [];
  responseMessage: string;
  showRegister: boolean = true;

  constructor(private fb: FormBuilder, private _route: ActivatedRoute, private formBuilder: FormBuilder,
    private router: Router,
    private restService: RestApiService) { }

  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      age: ['', Validators.required],
      favourite_colour: ['', [Validators.required, Validators.minLength(6)]],
      hobby: ['', Validators.required],
    });
    this._route.queryParams.subscribe(params => {
      this.personId = params['id'];
      var action = params['action'];
      this.fetchPersonById(this.personId);
      if (action == 'edit') {
        this.isEdit = true;
        this.isView = false;
        this.removable = true;
        this.selectable = true;
      }
      if (action == 'view') {
        this.isView = true;
        this.isEdit = false;
        this.removable = false;
        this.selectable = false;
      }
    });
  }
  selectable = true;
  removable = true;
  addOnBlur = true;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];

  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    if ((value || '').trim()) {
      this.hobbyList.push(value.trim());
    }
    if (input) {
      input.value = '';
    }
  }

  remove(ho: any[]): void {
    const index = this.hobbyList.indexOf(ho);
    if (index >= 0) {
      this.hobbyList.splice(index, 1);
    }
  }

  fetchPersonById(personId) {
    this.restService.getData(this.fetchByIdApiURL + personId).subscribe(
      res => {
        this.responseMessage = res.message;
        this.personDataArray = res.elements;
        if (this.personDataArray.length != 0) {
          this.hobbyList = res.elements[0].hobby;
          this.registerForm.setValue({
            first_name: res.elements[0].first_name,
            last_name: res.elements[0].last_name,
            age: res.elements[0].age,
            favourite_colour: res.elements[0].favourite_colour,
            hobby: res.elements[0].hobby
          });
        }

      });
  }

  hobbyList: any[] = [];
  updatePerson() {
    var obj = {
      "id": this.personId,
      "first_name": this.registerForm.value['first_name'], "last_name": this.registerForm.value['last_name'],
      "age": this.registerForm.value['age'],
      "favourite_colour": this.registerForm.value['favourite_colour'], "hobby": this.hobbyList
    }

      if (this.validateForm(obj)) {
      if(obj.age <= 0){
      window.alert("Please Enter Valid Age");
    }
    else{
    this.restService.postData(this.updateApiURL, obj).subscribe(
      res => {
        this.responseMessage = res.message;
        this.showRegister = false;
      });
    }
  
    }
    else{
      window.alert("Please Enter All The Fields");
    }
  
  }

  validateForm(obj): Boolean {
    if(obj.first_name == null || obj.first_name == ""){
      return false;
    }
    if(obj.last_name == null || obj.last_name == ""){
      return false;
    }
    if(obj.age == null || obj.age == "" ){
      return false;
    }
    if(obj.favourite_colour == null || obj.favourite_colour == "" ){
      return false;
    }
    if(obj.hobby == null || obj.hobby.length == 0 ){
      return false;
    }
    return true;
  }

}
