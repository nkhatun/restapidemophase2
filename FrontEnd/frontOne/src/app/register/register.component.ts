import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RestApiService } from '../rest-api.service';
import { MatChipInputEvent } from '@angular/material/chips';
import { COMMA, ENTER } from '@angular/cdk/keycodes';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  loading = false;
  submitted = false;
  apiURL = 'http://localhost:8080/app/api/addPerson';
  responseMessage: string;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private restService: RestApiService
  ) {

  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      age: ['', Validators.required],
      favourite_colour: ['', [Validators.required, Validators.minLength(6)]],
      hobby: ['', Validators.required],
    });
  }

  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];

  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;
    if ((value || '').trim()) {
      this.hobbyList.push(value.trim());
    }
    if (input) {
      input.value = '';
    }
  }

  remove(ho: any[]): void {
    const index = this.hobbyList.indexOf(ho);
    if (index >= 0) {
      this.hobbyList.splice(index, 1);
    }
  }

  hobbyList: any[] = [];
  showRegister: boolean = true;
  addPerson() {
    var obj = {
      "first_name": this.registerForm.value['first_name'], "last_name": this.registerForm.value['last_name'],
      "age": this.registerForm.value['age'],
      "favourite_colour": this.registerForm.value['favourite_colour'], "hobby": this.hobbyList
    }
    if (this.validateForm(obj)) {
      if(obj.age <= 0){
      window.alert("Please Enter Valid Age");
    }
    else{
    this.restService.postData(this.apiURL, obj).subscribe(
        res => {
          this.responseMessage = res.message;
          this.showRegister = false;
        });
    }
  
    }
    else{
      window.alert("Please Enter All The Fields");
    }
  }

  validateForm(obj): Boolean {
    if(obj.first_name == null || obj.first_name == ""){
      return false;
    }
    if(obj.last_name == null || obj.last_name == ""){
      return false;
    }
    if(obj.age == null || obj.age == "" ){
      return false;
    }
    if(obj.favourite_colour == null || obj.favourite_colour == "" ){
      return false;
    }
    if(obj.hobby == null || obj.hobby.length == 0 ){
      return false;
    }
    return true;
  }

}
