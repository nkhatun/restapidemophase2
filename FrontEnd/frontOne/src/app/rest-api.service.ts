import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError, tap, finalize, map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class RestApiService {
  apiURL = 'http://localhost:8080/app/api/fetchAllPerson';

  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Credentials': 'true'      
    })
  }


  public getData(url): Observable<any> {
    return this.http.get(url, this.httpOptions)
      .pipe(
        catchError(this.errorHandler)
      );
  }
    public postData(url: string, data: any): Observable<any>  {
    return this.http.post(url, data)
        .pipe(
          retry(3),
          catchError(this.errorHandler)
        );
  }

  errorHandler(error: HttpErrorResponse) {
    return throwError(error);
  }

}
