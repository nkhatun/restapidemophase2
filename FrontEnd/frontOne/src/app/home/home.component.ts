import { Component, OnInit } from '@angular/core';
import {RestApiService} from '../rest-api.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  myDataArray: any[] = [];
  fetchApiURL = 'http://localhost:8080/app/api/fetchAllPerson';
  deleteByIdApiURL = 'http://localhost:8080/app/api/deletePersonById?id=';
  responseMessage:string;
  displayedColumns: string[] = ['id','first_name','last_name','age','favourite_colour','hobby','action'];
  constructor(private restService: RestApiService) { }

  ngOnInit(): void {
   this.fetchPersonDetails(); 
  }

  fetchPersonDetails(){
    this.restService.getData(this.fetchApiURL).subscribe(
      res => {
       this.responseMessage = res.message;
       this.myDataArray = res.elements;
      });
  }

  onDelete(personId){
       if(window.confirm('Are sure you want to delete the person entry ?')){
         this.restService.getData(this.deleteByIdApiURL+personId).subscribe(
        res => {
       this.responseMessage = res.message;
       this.fetchPersonDetails();
      });
   }
       
  }

}
