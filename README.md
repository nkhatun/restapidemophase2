# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
This repository contains rest api backend in java (BackEnd) and front end integration in angular(FrontEnd).
BackEnd:
Java using spring boot application
FrontEnd:
Angular 9
RestAPIDemoTest.jar contains the excutable jar for the  overall integrated application.
To run the jar execute the below command(java must be installed in local machine):
1.java -jar RestAPIDemoTest.jar
2.Browse the application in http://localhost:8080/



### How do I get set up? ###
BackEnd:
1.Clone the backend application 
2.Import the project as existing maven application
3.Run as maven clean & maven install
4.maven - update project 
5.Run as spring boot app
To build an executable jar, execute the below command:
mvn spring-boot:run
Jar will be created under target folder, named as RestAPIDemoTest-0.0.1-SNAPSHOT.jar
To run the jar execute command:
java -jar <name of the jar>
Browse the backend application in http://localhost:8080/

FrontEnd:
1.Clone the application
2.Goto base directory
3.npm install @angular/cli
4.ng add @angular/material
5.run as:  ng serve
6.Browse the frontend application in http://localhost:4200/
To build the application, execute the below command:
ng build --prod --base-href .
dist folder is created inside the base directory

To Integrate The frontend application inside the back end application:
A.
1.copy the files of frontend dist to static folder of backend application
2.run the backend application as springboot application
3.Browse the frontend application in http://localhost:8080/
B.
1.Run both the application separately

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

 Repo owner or admin
* Other community or team contact